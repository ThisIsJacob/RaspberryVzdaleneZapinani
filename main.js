var fs = require('fs');
var Gpio = require('onoff').Gpio;
var http = require('http');
var url = require('url');

//konstanty
const webowka = '<html><head></head><body><p>Zapinani/Vypinani PC</p><button onclick="Stiskni()">Stisknout</button> Cas stisknuti: <input type="text" id="myText" value="500"><p id="demo"></p></body></html><script>function Stiskni(){var request = new XMLHttpRequest(); request.open(\'GET\', \'http://127.0.0.1:20800/tlacitko?cas=\'+document.getElementById("myText").value, true); request.onload = function () {document.getElementById("demo").innerHTML = this.response}; request.send(); }</script>';
const zapinaci_tlacitko = new Gpio(27, 'out');
const port_webu=20800;

//spuštění webserveru
http.createServer(function (req, res) {
    if(req.url=="/"){
        res.writeHead(200, {
            'Content-Type': 'text/html'
        });
        res.write(webowka);
        res.end();
    }
    else
    {
        console.log("ds");
        var q = url.parse(req.url, true).query;
        var cas="";
        cas = q.cas;
        res.writeHead(200, {
            'Content-Type': 'application/json'
        });
        var il=false;
        var letters = /^[A-Za-z]+$/;
        if (cas.match(letters)) il=true;
        if(cas==undefined | il==true){
            res.write('FAIL');
        }
        else
        {
            console.log("Cas: "+cas);
            stiskni_tlacitko(cas);
            res.write('OK');
        }
        res.end("");
    }
    console.log(req.url);
}).listen(port_webu)



function stiskni_tlacitko(najakdlouho){
    zapinaci_tlacitko.write(1);
    setTimeout(function(){
        zapinaci_tlacitko.write(0);
    },najakdlouho)
}